from django.conf import settings
from django.urls import re_path, include
from automationcommon import views as common


urlpatterns = [
    # service status page
    re_path(r'^status/20d47308-dd08-4aa6-991c-c46a6e7fced7/$', common.status, name='status-page'),
]


if hasattr(settings, 'IMPERSONATION_APPS'):
    urlpatterns += [
        re_path(r'^hijack/', include('hijack.urls')),
        re_path(r'^superadmin/impersonate', common.impersonate),
    ]
