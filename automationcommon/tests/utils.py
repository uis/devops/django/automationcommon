from bs4 import BeautifulSoup
from django.contrib.auth.models import User
# django.core.urlresolvers has been deprecated since 1.10 and removed in 2.0
from django.test import TestCase
from django.test import override_settings
from mock import patch


def assert_contains_in_order(actual, expected):
    """
    assert that all the strings in *expected* occur in the *actual* in the order in which they are listed

    :param actual: target string to search
    :param expected: collection of strings to search for
    """
    for query in expected:
        query = str(query)
        index = actual.find(query)
        if index == -1:
            raise AssertionError(f"{query} not found in target")
        actual = actual[index + len(query):]


user_dict = {
    'test0001': 'Testy Mike',
    'it123': 'Ivanna Tinkle',
    'ac123': 'Al Coholic',
    'hr123': 'Harry Rump',
    'bl123': 'Bill Loney',
    'jk123': 'Joe King'
}


PASSWORD = 'notsecret'


@override_settings(CELERY_EAGER_PROPAGATES_EXCEPTIONS=True, CELERY_ALWAYS_EAGER=True, BROKER_BACKEND='memory',
                   DEBUG=True)
class UnitTestCase(TestCase):

    def setUp(self):

        def return_visible_name_by_crsid_side_effect(*args):
            return user_dict[args[0]]

        return_visible_name_by_crsid = self.patch("ucamlookup.signals.return_visibleName_by_crsid")
        return_visible_name_by_crsid.side_effect = return_visible_name_by_crsid_side_effect

    def do_test_login(self, username, superuser=False):
        """
        Do a fake login.

        :param username: username
        :param superuser: whether or not a super user
        :return: logged in user
        """
        if username in user_dict:
            user = User.objects.create_superuser(username, f"{username}@cam.ac.uk", PASSWORD) \
                if superuser else User.objects.create_user(username, password=PASSWORD)
            self.client.login(username=username, password=PASSWORD)
            return user

    def do_admin_login(self, username):
        """
        Do a fake adminlogin.

        :param username: username
        :return: logged in admin user
        """
        user = self.do_test_login(username)
        user.is_superuser = True
        user.save()
        return user

    def get_html_test_target(self, response):
        """
        Returns the test_target div for a valid html response encapsulated with BeautifulSoup.

        :param response: http response
        :return: a BeautifulSoup instance

        """
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], "text/html; charset=utf-8")
        return BeautifulSoup(response.content, 'html.parser').find("div", {"class": "test_target"})

    def assert_element_with_text(self, target, element, text):
        """
        assert that an HTML element with text can be found in target

        :param target: bs4 html tree
        :param element: element to find
        :param text: text in element to match
        """
        for el in target.find_all(element):
            if el.get_text().strip() == text:
                return
        self.fail(f"Couldn't find element {element} with text {text}")

    def patch(self, target_name, side_effect=None):
        """
        Patching helper method

        :param target_name: name of target method to patch
        :param side_effect: optional side_effect to set
        :return: the patched target method
        """
        patcher = patch(target_name)
        target = patcher.start()
        self.addCleanup(patcher.stop)
        if side_effect:
            target.side_effect = side_effect
        return target
