#!/usr/bin/env python

from setuptools import find_packages, setup

setup(
    name='django-automationcommon',
    version='2.0.0',
    packages=find_packages(),
    include_package_data=True,
    license='MIT',
    description=(
        'Common functionality across different Django projects of the UofC UIS Automation team'
    ),
    long_description=open('README.rst').read(),
    url='https://github.com/uisautomation/django-automationcommon.git',
    author='DevOps team, University Information Services, University of Cambridge',
    author_email='devops@uis.cam.ac.uk',
    tests_require=['testfixtures'],
    install_requires=[
        'django>=3.2',
        'django-ucamlookup>=3.0.5,<4',
        'requests',
        'celery',
        'python-dateutil',
        'django-hijack',
        'django-stronghold',
        'beautifulsoup4',
        'mock',
    ],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
    ],
)
